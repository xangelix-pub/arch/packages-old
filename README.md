# AUR Packages

## linux-vfio

[AUR **NOT OWNED**](https://aur.archlinux.org/packages/linux-vfio)

[Upstream](https://github.com/torvalds/linux)

[Default Arch Kernel Config](https://raw.githubusercontent.com/archlinux/svntogit-packages/packages/linux/trunk/config)

[Patched File](https://github.com/archlinux/linux/blob/master/drivers/pci/quirks.c)

## element-desktop-git

[AUR](https://aur.archlinux.org/packages/element-desktop-git)

[Upstream](https://github.com/vector-im/element-desktop)

## element-web-git

[AUR](https://aur.archlinux.org/packages/element-web-git)

[Upstream](https://github.com/vector-im/element-web)

## geesefs-bin

[AUR](https://aur.archlinux.org/packages/geesefs-bin)

[Upstream](https://github.com/yandex-cloud/geesefs)

## geesefs-git

[AUR](https://aur.archlinux.org/packages/geesefs-git)

[Upstream](https://github.com/yandex-cloud/geesefs)

## steam-rom-manager-appimage

[AUR](https://aur.archlinux.org/packages/steam-rom-manager-appimage)

[Upstream](https://github.com/SteamGridDB/steam-rom-manager)

## steam-rom-manager-git

[AUR](https://aur.archlinux.org/packages/steam-rom-manager-git)

[Upstream](https://github.com/SteamGridDB/steam-rom-manager)
